using Assets.Scripts.Helpers;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using static Assets.Scripts.Helpers.ChanceCalculator;
using static PersonScript;

public class SimulationManager : MonoBehaviour
{
    [System.Serializable]
    public class StatisticsList
    {
        public int TotalDeath;
        public Statistics[] list;
    }

    [System.Serializable]
    public class Statistics
    {
        public int S; 
        public int I; 
        public int R;
        public int Isolation;
        public int Death;
        public float Time;
        public float R0;
    }

    private static SimulationManager instance;
    public static SimulationManager Instance
    {
        get
        {
            if (Instance == null)
            {
                instance = FindObjectsOfType<SimulationManager>()[0];
            }
            return instance;
        }
    }
    [Header("Nazwa")]
    [SerializeField]
    public string SimulationName;

    #region Parameters
    [Header("Parameters")]
    [SerializeField]
    public int InfectionRate;

    [SerializeField]
    float InfectionDistance;

    [SerializeField]
    bool SocialDistanceActive;

    [SerializeField]
    float SocialDistanceValue;

    [SerializeField]
    float ImmueTime;

    [SerializeField]
    float InfectionTime;

    [SerializeField]
    int PopulationNumber;

    [SerializeField]
    int IllnessNumber;

    [SerializeField]
    float PersonSpeed;

    [Header("Death")]
    [SerializeField]
    public bool TurnDeath;

    [SerializeField]
    public int DeathChance;

    [Header("Vaccination")]
    [SerializeField]
    public bool StartVaccination;

    [SerializeField]
    public int VaccinationPopulation;

    [Header("Travel")]
    [SerializeField]
    public bool RunTravel;

    [SerializeField]
    public float TravelInterval;

    [Header("Isolation")]
    [SerializeField]
    public bool RunIsolation;

    [SerializeField]
    public Percent IsolationPercent;
    #endregion

    #region Statistics
    [Header("Statistics")]
    [SerializeField]
    int Susceptiible;

    [SerializeField]
    int Infectious;

    [SerializeField]
    int Removed;

    [SerializeField]
    int Dead;

    [SerializeField]
    int Vaccinated;

    [SerializeField]
    float R0;

    [SerializeField]
    Dictionary<PersonScript,int> counters;
    #endregion

    [Header("Script Fields")]
    [SerializeField]
    List<Area> areas;

    [SerializeField]
    Area Isolation;

    [SerializeField]
    List<PersonScript> persons;


    [SerializeField]
    PersonFactory factory;

    [SerializeField]
    private Timer TravelTimer;

    [SerializeField]
    Dictionary<PersonScript, Area> PersonsInIsolation;

    [SerializeField]
    bool SAVED = false;

    [Header("Infos")]
    [SerializeField]
    Text infoS;

    [SerializeField]
    Text infoI;

    [SerializeField]
    Text infoR;

    [SerializeField]
    Text infoD;

    [SerializeField]
    Text infoIsolation;

    [SerializeField]
    Text infoR0;

    [SerializeField]
    public List<Statistics> statistics;

    float time = 0;
    private int newInfectious;

    public bool Run = false;

    // Start is called before the first frame update
    void Start()
    {
        SetUpObjects();
        CreatePersons();
        SetUpArea();
        InfectPerson();
        if (StartVaccination)
            Vaccinatedpeople();
        Run = true;
    }

    private void SetUpObjects()
    {
        counters = new Dictionary<PersonScript, int>();
        PersonsInIsolation = new Dictionary<PersonScript, Area>();
        statistics = new List<Statistics>();

        TravelTimer.SetTime(TravelInterval);
        TravelTimer.Run();
    }

    // Update is called once per frame
    void Update()
    {
        time += Time.deltaTime;         
        ShowInfo();
        ObserveStatistics();
        RandomPersonToMove();
    }

    private void ShowInfo()
    {
        infoS.text = "S:" + Susceptiible.ToString();
        infoI.text = "I:" + Infectious.ToString();
        infoR.text = "R:" + Removed.ToString();
        infoIsolation.text = "Iso:" + Isolation.ToString();
        infoD.text = "Dead:" + Dead.ToString();
        infoR0.text = "R0:" + R0.ToString();
    }

    private void ObserveStatistics()
    {
        statistics.Add(new Statistics() { S = Susceptiible, I = Infectious, R = Removed, Time = time, R0 = R0, Death =Dead, Isolation = Isolation.PeopleInside.Count });
        if (Infectious <2 && time >20 && !SAVED)
        {            
            SAVED = true;
            JSONHelper.SaveDataJSON(SimulationName, new StatisticsList() { list = statistics.ToArray(), TotalDeath = Dead });
        }
    }

    public void CreatePersons()
    {
        if (!StartVaccination)
            VaccinationPopulation = 0;

        Susceptiible = PopulationNumber;

        PersonParameters personParameters = new PersonParameters();

        personParameters.DistanceRange = SocialDistanceValue;
        personParameters.SicknessRange = InfectionDistance;
        personParameters.SicnkessTime = InfectionTime;
        personParameters.InfectionRate = InfectionRate;
        personParameters.Speed = PersonSpeed;

        if (SocialDistanceActive)
            personParameters.MovementType = Assets.Scripts.Movement.MovementModule.KeepDistance;
        else
            personParameters.MovementType = Assets.Scripts.Movement.MovementModule.Random;

        persons.AddRange(factory.Create(PopulationNumber, personParameters));
        persons.ForEach(x => x.SimulationManager = this);
    }

    public void InfectPerson()
    {

        for (int i = 0; i < IllnessNumber; i++)
        {
            PersonScript personScript;
            do
            {
                int choosen = Random.Range(0, PopulationNumber);
                personScript = persons[choosen];
            } while (personScript.status.Equals(Status.Ill) || personScript.status.Equals(Status.Immue));
            personScript.ChangeStatus(Status.Ill);
        }
    }
    public void Vaccinatedpeople()
    {

        for (int i = 0; i < VaccinationPopulation; i++)
        {
            PersonScript personScript;
            do
            {
                int choosen = Random.Range(0, PopulationNumber);
                personScript = persons[choosen];
            } while (personScript.status.Equals(Status.Ill) || personScript.status.Equals(Status.Immue));
            personScript.ChangeStatus(Status.Immue);
        }
    }

    private bool IsMultiAreaTravel()
    {
        return RunTravel && areas.Count > 1;
    }

    private void RandomPersonToMove()
    {
        if (RunTravel && TravelTimer.IsTime())
        {
            if(IsMultiAreaTravel())
            {
                MoveToAnother();
            }
            else
            {
                MoveToCenter();
            }

            TravelTimer.Reset();
        }
    }

    private void MoveToAnother()
    {
        int personIndex = Random.Range(0, persons.Count);
        var newListArea = areas.Where(l => l != persons[personIndex].Map).ToList();
        int areaIndex = Random.Range(0, newListArea.Count);

        var person = persons[personIndex];
        var area = (newListArea[areaIndex]);
        person.SetUpArea(area);
        person.transform.position = area.GetRandomPosition();
        person.Active();
    }

    private void MoveToCenter()
    {
        int personIndex = Random.Range(0, persons.Count);
        var person = persons[personIndex];
        var area = areas.First();

        person.SetUpArea(area);
        person.transform.position = area.GetRandomCenterPosition();
        person.Active();
    }

    private void HandleIsolation(PersonScript person)
    {
        if(Run && RunIsolation && ChanceCalculator.CheckChance(newInfectious, IsolationPercent))
        {
            MoveToIsolation(person);
        }
    }

    private void MoveToIsolation(PersonScript person)
    {
        PersonsInIsolation.Add(person, person.Map);
        person.SetUpArea(Isolation);
        person.transform.position = Isolation.GetRandomPosition();
        person.Active();
    }

    private void TakeBackFromIsolation(PersonScript person)
    {
        if(RunIsolation && PersonsInIsolation.Keys.Contains(person))
        {
            var area = PersonsInIsolation[person];
            person.SetUpArea(area);
            person.transform.position = area.GetRandomPosition();
            person.Active();
            PersonsInIsolation.Remove(person);
        }
    }

    public void SetUpArea()
    {
        foreach (var person in persons)
        {
            int areaIndex = Random.Range(0, areas.Count);
            person.SetUpArea(areas[areaIndex]);
            person.transform.position = areas[areaIndex].GetRandomPosition();
            person.Active();
        }
    }


    public void NoticePersonChange(PersonScript personScript, Status oldStatus, Status newStatus)
    {
        switch (oldStatus)
        {
            case Status.Healthy:
                Susceptiible--;
                break;
            case Status.Ill:
                Infectious--;
                TakeBackFromIsolation(personScript);
                break;
            case Status.Immue:                
                Removed--;
                break;
            default:
                break;
        }

        switch (newStatus)
        {
            case Status.Healthy:
                Susceptiible++;
                break;
            case Status.Ill:
                Infectious++;
                newInfectious++;
                HandleIsolation(personScript);
                break;
            case Status.Immue:
                Removed++;
                break;
            case Status.Death:
                Dead++;
                break;
            default:
                break;
        }
    }

    public void AddCounter(PersonScript personScript, int counter)
    {
        counters[personScript] = counter;
        float sum = counters.Values.Sum();
        R0 = (float)counters.Values.Sum() / (float)counters.Values.Count;
    }
}
