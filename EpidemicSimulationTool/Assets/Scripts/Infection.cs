﻿using UnityEngine;
using static PersonScript;

namespace Assets.Scripts
{
    public class Infection : MonoBehaviour, IActive
    {
        [SerializeField]
        Timer ContactTimer;

        [SerializeField]
        public Timer SicknessTimer;

        [SerializeField]
        ParticleSystem Effect;

        [SerializeField]
        PersonScript Parent;

        [SerializeField]
        bool active;

        [SerializeField]
        public int counter = 0;


        public bool IsActive
        {
            get
            {

                return IsActive;
            }
            set
            {
                if (value)
                {
                    SicknessTimer.Reset();
                    SicknessTimer.Run();
                    Effect.Play();
                }             
                else
                {
                    SicknessTimer.Stop();
                    Effect.Stop();
                }

                active = value;
            }
        }

        private bool CheckPersonImmue(PersonScript personScript)
        {
            return !personScript.IsActive || personScript.status == Status.Immue || personScript.status == Status.Ill || personScript.status == Status.Death || personScript.Map != Parent.Map;
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (!active || Parent.Isolation)
                return;

            ContactTimer.Reset();
            ContactTimer.Run();
        }

        void OnTriggerStay2D(Collider2D col)
        {
            if (!active || Parent.Isolation)
                return;

            if (ContactTimer.IsTime())
            {
                var person = col.gameObject.GetComponent<PersonScript>();
                if (person == null)
                    person = col.gameObject.GetComponentInParent<PersonScript>();

                if (CheckPersonImmue(person))
                    return;

                ContactTimer.Reset();
                float temp = Random.Range(1, 100);
                if (temp <= 10 + Parent.SimulationManager.InfectionRate)
                {
                    counter++;
                    Parent.SimulationManager.AddCounter(Parent, counter);
                    person.ChangeStatus(Status.Ill);
                }
            }
        }

        private void OnTriggerExit2D(Collider2D collision)
        {
            if (!active)
                return;

            ContactTimer.Stop();
            ContactTimer.Reset();
        }

        public void Synchronize()
        {
            SicknessTimer.SetTime(Parent.Settings.SicnkessTime);
            GetComponent<CircleCollider2D>().radius = Parent.Settings.SicknessRange;
        }
    }
}
