﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.Helpers
{
    public static class ChanceCalculator
    {
        public enum Percent
        {
            T10,
            T20,
            T30,
            T40,
            T60,
            T70,
            T80,
            T90,          
            T25,
            T75,
            T50,         
            T100
        }

        public static bool CheckChance(float chance)
        {
            float temp = Random.Range(1, 100);
            return temp <= 1 + chance;
        }

        public static bool CheckChance(int iterator, float chance)
        {
            var tet = (100 / chance);
            return iterator % tet == 0;
        }

        public static bool CheckChance(int iterator, Percent percent)
        {
            int baseNumber = ReturnBase(percent);
            int fraction = ReturnFraction(percent);
            var test = iterator % baseNumber;
            return test < fraction;
        }

        private static int ReturnBase(Percent percent)
        {
            switch (percent)
            {
                case Percent.T10:
                    return 10;
                case Percent.T20:
                    return 10;
                case Percent.T30:
                    return 10;
                case Percent.T40:
                    return 10;
                case Percent.T60:
                    return 10;
                case Percent.T70:
                    return 10;
                case Percent.T80:
                    return 10;
                case Percent.T90:
                    return 10;
                case Percent.T25:
                    return 4;
                case Percent.T75:
                    return 4;
                case Percent.T50:
                    return 2;
                case Percent.T100:
                    return 1;
                default:
                    return 0;
            }
        }

        private static int ReturnFraction(Percent percent)
        {
            switch (percent)
            {
                case Percent.T10:
                    return 1;
                case Percent.T20:
                    return 2;
                case Percent.T30:
                    return 3;
                case Percent.T40:
                    return 4;
                case Percent.T60:
                    return 6;
                case Percent.T70:
                    return 7;
                case Percent.T80:
                    return 8;
                case Percent.T90:
                    return 9;
                case Percent.T25:
                    return 1;
                case Percent.T75:
                    return 3;
                case Percent.T50:
                    return 1;
                case Percent.T100:
                    return 1;
                default:
                    return 0;
            }
        }
    }
}
