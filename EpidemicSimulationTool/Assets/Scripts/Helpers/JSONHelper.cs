﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using static SimulationManager;

namespace Assets.Scripts.Helpers
{
    public static class JSONHelper
    {
        public static string ConvertToJSON(StatisticsList myObject)
        {
            return JsonUtility.ToJson(myObject);
        }

        public static void SaveDataJSON(string name, StatisticsList myObject)
        {
            string text = ConvertToJSON(myObject);
            FileHelper.SaveData(string.Format("{0}.json", name), text);
        }
    }
}
