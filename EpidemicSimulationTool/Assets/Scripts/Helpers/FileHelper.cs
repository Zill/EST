﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Net.Mime.MediaTypeNames;

namespace Assets.Scripts.Helpers
{
    public class FileHelper
    {
        public static void SaveData(string fileName, string text)
        {
            File.WriteAllText(fileName, text);
        }
    }
}
