﻿namespace Assets.Scripts
{
    public interface IActive
    {
        bool IsActive { get; set; }
    }
}
