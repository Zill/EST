using Assets.Scripts;
using Assets.Scripts.Helpers;
using Assets.Scripts.Movement;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public enum SubModules
{
    Contact,
    Sickness
}

public class PersonScript : MonoBehaviour
{
    [SerializeField]
    private Movement movementModule;

    [SerializeField]
    public bool IsActive;

    [SerializeField]
    List<IActive> activesComponentsList;

    public enum Status
    {
        Healthy,
        Ill,
        Immue,
        Vaccinated,
        Death
    }

    [SerializeField]
    public Status status;
    [SerializeField]
    public Infection infectionModule;
    [SerializeField]
    public Area Map;
    [SerializeField]
    public Vector2 target;

    [SerializeField]
    PersonParameters settings;

    [SerializeField]
    public bool Isolation;

    [SerializeField]
    public SimulationManager SimulationManager;

    [SerializeField]
    Timer DeathTimer;

    public PersonParameters Settings { get { return settings; } }


    public struct PersonParameters
    {
        public float Speed { get; set; }
        public MovementModule MovementType { get; set; }
        public float SicnkessTime { get; set; }
        public float SicknessRange { get; set; }
        public float DistanceRange { get; set; }
        public int InfectionRate { get; set; }
        public int ImueTime { get; set; }
    }

    public void Initializate(PersonParameters parameters)
    {
        settings = parameters;

        if (movementModule != null)
            movementModule.Synchronize();
        if (infectionModule != null)
            infectionModule.Synchronize();

        activesComponentsList = GetComponents<IActive>().ToList();
        activesComponentsList.AddRange(GetComponentsInChildren<IActive>());
    }

    public void SetUpArea(Area area)
    {
        Map?.PeopleInside.Remove(this);

        Map = area;
        Map.PeopleInside.Add(this);

        IsActive = true;
        if (movementModule != null)
            movementModule.Reset();
    }

    public void Active()
    {
        IsActive = true;
        if (movementModule != null)
            movementModule.IsActive = true;
    }

    public void Deactive()
    {
        IsActive = false;

        foreach (var activeComponent in activesComponentsList)
        {
            activeComponent.IsActive = false;
        }
    }

    void ChangeColor()
    {
        switch (status)
        {
            case Status.Healthy:
                GetComponent<SpriteRenderer>().color = Color.white;
                return;
            case Status.Ill:
                GetComponent<SpriteRenderer>().color = Color.red;
                return;
            case Status.Immue:
                GetComponent<SpriteRenderer>().color = Color.gray;
                return;
            case Status.Death:
                GetComponent<SpriteRenderer>().color = Color.black;
                return;
            default:
                return;
        }
    }

    void CheckDeath()
    {
       if( SimulationManager.TurnDeath && DeathTimer.IsTime())
       {
            DeathTimer.Reset();
            if (ChanceCalculator.CheckChance(SimulationManager.DeathChance))
            {
                ChangeStatus(Status.Death);
                infectionModule.SicknessTimer.Stop();
                infectionModule.SicknessTimer.Reset();
            }            
       }
    }

    // Update is called once per frame
    void Update()
    {
        if (IsActive)
        {          
            if ( status.Equals(Status.Ill))
            {
                CheckDeath();

                if (infectionModule.SicknessTimer.IsTime())
                {
                    ChangeStatus(Status.Immue);
                }          
            }

            ChangeColor();

            if (Map != null && movementModule != null)
                movementModule.Move();
        }
    }

    public void ChangeStatus(Status status)
    {
        if (this.status.Equals(status))
            return;

        if (SimulationManager != null)
            SimulationManager.NoticePersonChange(this, this.status, status);

        this.status = status;
        switch (this.status)
        {
            case Status.Healthy:
                break;
            case Status.Ill:
                infectionModule.IsActive = true;
                DeathTimer.Run();
                break;
            case Status.Immue:
                break;
            default:
                infectionModule.IsActive = false;
                break;
        }
    }
}
