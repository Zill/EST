using System.Collections.Generic;
using UnityEngine;

public class Area : MonoBehaviour
{
    [SerializeField]
    Area center;

    [SerializeField]
    public List<PersonScript> PeopleInside;

    public Transform Transform { get { return this.transform; } }

    public Vector2 GetRandomPosition()
    {
        float x = Random.Range(transform.position.x - transform.localScale.x / 2, transform.position.x + transform.localScale.x / 2);
        float y = Random.Range(transform.position.y - transform.localScale.y / 2, transform.position.y + transform.localScale.y / 2);

        return new Vector2(x, y); 
    }

    public Vector2 GetRandomCenterPosition()
    {
        float x = Random.Range(transform.position.x - transform.localScale.x / 20, transform.position.x + transform.localScale.x / 20);
        float y = Random.Range(transform.position.y - transform.localScale.y / 20, transform.position.y + transform.localScale.y / 20);

        return new Vector2(x, y);
    }
}
