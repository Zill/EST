using Assets.Scripts.Movement;
using System.Collections.Generic;
using UnityEngine;
using static PersonScript;

public class PersonFactory : MonoBehaviour
{
    [SerializeField]
    private PersonScript personPrefab;

    public List<PersonScript> Create(int personNumber, PersonParameters parameters)
    {
        List<PersonScript> persons = new List<PersonScript>();
        for (int i = 0; i < personNumber; i++)
        {
            persons.Add(CreateInstance(personPrefab, parameters));
        }

        return persons;
    }

    private PersonScript CreateInstance(PersonScript prefab, PersonParameters parameters)
    {
        PersonScript newPerson = Object.Instantiate(prefab);
        
        newPerson.Initializate(parameters);
        return newPerson;
    }
}
