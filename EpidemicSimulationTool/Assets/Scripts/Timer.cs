using UnityEngine;

public class Timer : MonoBehaviour
{
    // Start is called before the first frame update
    public float TargerTime;
    public float CurrentTime;
    public bool Running;


    public void SetTime(float targetTime)
    {
        TargerTime = targetTime;
        Reset();
    }

    public void Reset()
    {
        CurrentTime = 0;
    }

    public void Run()
    {
        Running = true;
    }

    public void Stop()
    {
        Running = false;
    }

    // Update is called once per frame
    void Update()
    {
        if(Running)
        CurrentTime += Time.deltaTime;
    }

    public bool IsTime()
    {
        return CurrentTime > TargerTime;
    }
}
