﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Movement
{

    public class Movement : MonoBehaviour, IActive
    {
        [SerializeField]
        private bool active;

        [SerializeField]
        private PersonScript Parent;

        #region BasicParameters
        [Header("Basic Parameters")]

        [SerializeField]
        private Vector2 currentTarget;

        [SerializeField]
        private float speed;

        [SerializeField]
        private MovementModule Mode;
        #endregion

        #region SocialDistance
        [Header("Social Distance")]
        [SerializeField]
        List<GameObject> invadersMustDie;

        [SerializeField]
        bool change = false;

        [SerializeField]
        private float targetDistance;
        #endregion


        public bool IsActive { get { return active; } set { active = value; } }

        public void Start()
        {
            invadersMustDie = new List<GameObject>();
        }

        public void ChangeMode(MovementModule mode)
        {
            Mode = mode;

            switch (mode)
            {
                case MovementModule.Random:
                    break;
                case MovementModule.KeepDistance:
                    break;
                default:
                    break;
            }
        }

        public void Move()
        {
            if (!active)
            {
                return;
            }

            if (Mode.Equals(MovementModule.KeepDistance) && invadersMustDie.Count > 0)
            {
                if (change)
                {
                    change = false;

                    Vector3 temp = gameObject.transform.position;
                    foreach (GameObject person in invadersMustDie)
                    {
                        var direction = (Parent.transform.position - person.transform.position).normalized;
                        var distance = Vector2.Distance(Parent.transform.position, person.transform.position);
                        var target = direction * (targetDistance - distance);

                        temp += (target / invadersMustDie.Count);
                    }
                    var mapTransform = Parent.Map;

                    float minX = mapTransform.transform.position.x - mapTransform.transform.localScale.x / 2;
                    float minY = mapTransform.transform.position.y - mapTransform.transform.localScale.y / 2;
                    float maxX = mapTransform.transform.position.x + mapTransform.transform.localScale.x / 2;
                    float maxY = mapTransform.transform.position.y + mapTransform.transform.localScale.y / 2;

                    temp.x = Random.Range(0.95f * temp.x, 1.05f * temp.x);
                    temp.y = Random.Range(0.95f * temp.y, 1.05f * temp.y);

                    if (temp.x < minX)
                    {
                        temp.x = minX;
                    }

                    if (temp.y < minY)
                    {
                        temp.y = minY;
                    }

                    if (temp.x > maxX)
                    {
                        temp.x = maxX;
                    }

                    if (temp.y > maxY)
                    {
                        temp.y = maxY;
                    }


                    currentTarget = temp;
                }
                else
                {
                    float step = speed * Time.deltaTime;
                    Parent.transform.position = Vector2.MoveTowards(Parent.transform.position, currentTarget, step);
                }
            }
            else if (System.Math.Round(Vector2.Distance(Parent.transform.position, currentTarget), 4) == 0)
            {
                currentTarget = Parent.Map.GetRandomPosition();
            }
            else
            {
                float step = speed * Time.deltaTime;
                Parent.transform.position = Vector2.MoveTowards(Parent.transform.position, currentTarget, step);
            }
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (!IsActive)
                return;

            var module = collision.GetComponentInParent<PersonScript>();

            if (module != null)
            {
                invadersMustDie.Add(collision.gameObject);
                change = true;
            }
        }

        private void OnTriggerExit2D(Collider2D collision)
        {
            if (!IsActive)
                return;

            var module = collision.GetComponentInParent<PersonScript>();

            if (module != null)
            {
                invadersMustDie.Remove(collision.gameObject);
                change = true;
            }
        }

        public void Reset()
        {
            if(Parent.Map != null)
            currentTarget = Parent.Map.GetRandomPosition();
            change = false;
            invadersMustDie = new List<GameObject>();
        }

        public void Synchronize()
        {
            Mode = Parent.Settings.MovementType;
            speed = Parent.Settings.Speed;
            var collider = GetComponent<CircleCollider2D>();
            collider.radius = Parent.Settings.DistanceRange;
            targetDistance = Parent.Settings.DistanceRange;

            Reset();
        }
    }
}
