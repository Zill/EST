
public interface IMovement 
{
    void Move();

    void SetUp(PersonScript parent);

    void Reset();

    void Synchronize();
}
