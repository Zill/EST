﻿using Assets.Scripts;
using UnityEngine;

public class RandomMovement : MonoBehaviour, IMovement, IActive
{
    [SerializeField]
    private Vector2 currentTarget;

    [SerializeField]
    private float speed;
    [SerializeField]
    private PersonScript Parent;

    public float Speed { set { speed = value; } }

    [SerializeField]
    private bool active;
    public bool IsActive { get => active; set => active = value; }

    public void Move()
    {
        if(!active)
        {
            return;
        }

        if (System.Math.Round(Vector2.Distance(Parent.transform.position, currentTarget), 4) == 0)
        {
            currentTarget = Parent.Map.GetRandomPosition();
        }
        else
        {
            float step = speed * Time.deltaTime;
            Parent.transform.position = Vector2.MoveTowards(Parent.transform.position, currentTarget, step);
        }
    }

    public void SetUp(PersonScript personScript)
    {
        Parent = personScript;
    }

    public void Reset()
    {
        currentTarget = Parent.Map.GetRandomPosition();
    }

    public void Synchronize()
    {
        speed = Parent.Settings.Speed;
        var collider = GetComponent<CircleCollider2D>();
        collider.radius = Parent.Settings.DistanceRange;
        Reset();
    }
}
