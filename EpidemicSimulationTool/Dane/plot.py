import os
import json
import matplotlib.pyplot as plt


SLabel = "Susceptible";
ILabel = "Infected";
RLabel = "Removed";
R0Label = "R0";
IsolationLabel = "Isolated";
SimpleMap = 'SimpleMap'

SimulationName = "MultiMap-Vacination90"
SimulationName2 = "MultiMap-Vaccination90"
# open and parse InsetChart.json
ic_json = json.loads( open( SimulationName+'.json'  ).read() )

for x in  ic_json["list"]:
  print(x)
  
S = []
I = []
R = []
Isolation = []
time = []
for x in  ic_json["list"]:
  S.append(x["S"])
  I.append(x["I"])
  R.append(x["R"])
  Isolation.append(x["Isolation"])
  time.append(x["Time"])




# plot "Births" channel by time step
plt.plot(  time, S,'y-', label = SLabel)
plt.plot(  time, I, 'r-', label = ILabel)
plt.plot(  time, R, 'b-', label = RLabel )
#plt.plot(  time, Isolation, 'g-', label = IsolationLabel)
plt.xlabel("Simulation Time")
plt.ylabel("Persons")
plt.title( SimulationName2)
plt.grid(axis = 'y')
plt.legend()
plt.savefig('C:\\Users\\Admin\\Desktop\\Magisterka\\Dane\\Est\\'+SimulationName2+'.png')
plt.show()
